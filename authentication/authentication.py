# pylint: disable=missing-function-docstring

from flask import request
import jwt
import requests
from config.keycloak_config import keycloak_host, keycloak_realm
from reusable.log_message import logging


def authenticate():
    token = request.headers.get('Authorization')
    if not token:
        return None

    try:
        response = requests.get("http://" + keycloak_host + "/auth/realms/" + keycloak_realm)
        data = response.json()
        public_key = ("-----BEGIN PUBLIC KEY-----\n" + data['public_key'] + "\n-----END PUBLIC KEY-----")
        payload = jwt.decode(token[7:], public_key, algorithms=["RS256"], audience="account")
        return payload['preferred_username']
    except jwt.ExpiredSignatureError as e:
        logging.error(e)
        return None
    except jwt.PyJWTError as e:
        logging.error(e)
        return None
