# Fragebogen zum Programmentwurf Webengineering 2 DHBW Karlsruhe TINF21B3

## Gruppeninformationen

Gruppenname: TimoFelix

Gruppenteilnehmer:

- Felix Steinhauser
- Timo Huter

[Repository](https://gitlab.com/Raos1/tife)

## Lizenz

[MIT License]


## Sprache und Framework

| Frage                                 | Antwort                                            |
|---------------------------------------|----------------------------------------------------|
| Programmiersprache                    | python                                             |
| Sprachversion                         | 3.12                                               |
| Version ist aktuell und wird gepflegt | [X]                                                |
| Framework (FW)                        | "Flask"                                            |
| FW-Version                            | v3.0.0                                             |
| FW-Version ist aktuell                | [X]                                                |
| Website zum FW                        | [Flask](https://flask.palletsprojects.com/)        |
| Prepared statements/ORM               | "prepared statements"                              |
| ORM Version                           | not needed                                         |
| ORM Version ist aktuell               | []                                                |
| Website zum ORM                       | []                                                |

## Automatisierung

Art der Automatisierung: "GitLab CI"

## Testautomatisierung

Art der Testautomatisierung: "linting"


## Authentifizierung

* JWT wird berücksichtigt: [X]
* Signatur wird geprüft: [X]

## Konfiguration und Dokumentation

* Dokumentation existiert in bedarfsgerechtem Umfang: [X]
* Konfigurationsparameter sind sinnvoll gewählt: [X]
* keine hardcoded Zugänge zu angeschlossenenen Systemen (URLs, Passwörter, Datenbanknamen, etc.): [X]
* Umgebungsvariablen und Konfurationsdateien sind gelistet und beschrieben: [X]

## Logging
* Logsystem des Frameworks oder Biliothek wurde genutzt: [X]
* Logs enthalten alle geforderten Werte: [X]
* LogLevel ist konfigurierbar: [X]