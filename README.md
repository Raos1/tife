# tife

## Voraussetzungen

### Podman & kind

- WSL muss installiert sein
- Podman muss installiert sein und eine Machine zur Verfügung stehen
- Ein kind-Cluster muss betriebsbereit sein

### Biletado-Installation

[Installationsanleitung auf dem offiziellen biletado-gitlab-repository](https://gitlab.com/biletado/quickstart#install-biletado)

```shell
kubectl create namespace biletado
kubectl config set-context --current --namespace biletado
kubectl apply -k https://gitlab.com/biletado/kustomize.git//overlays/kind?ref=main --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

basiert auf der [Installationsanleitung auf dem offiziellen biletado-gitlab-repository](https://gitlab.com/biletado/quickstart#install-biletado)

### Auführung des Codes in einer Entwicklungsumgebung

Der Code kann in einer Python-Entwicklungsumgebung ausgeführt werden, indem die [main.py](https://gitlab.com/Raos1/tife/-/tree/main/main.py) mit Python3.12 ausgeführt wird.
Einige Umgebungsvariablen müssen in der Run-Konfiguration angegeben werden, beispielsweise so:
```runner
PYTHONUNBUFFERED=1;KEYCLOAK_HOST=localhost:9090;KEYCLOAK_REALM=biletado;POSTGRES_ASSETS_USER=postgres;POSTGRES_ASSETS_PASSWORD=postgres;POSTGRES_ASSETS_DBNAME=assets;POSTGRES_ASSETS_HOST=localhost;POSTGRES_ASSETS_PORT=5432;RESERVATIONS_API_HOST=http://reservations/
```
Eine Datenbank mit Port-Forwarding sollte im Voraus bereitgestellt sein.
Befehl für Port-Forwarding:
```shell
kubectl port-forward services/postgres 5432:5432
```

### Containererstellung

Die Containererstellung ist im Directory der Implementierung durchzuführen.

```shell
podman build -t registry.gitlab.com/biletado/quickstart:local-dev .
podman save registry.gitlab.com/biletado/quickstart:local-dev --format oci-archive -o quickstart.tar
kind load image-archive quickstart.tar -n kind-cluster
kubectl apply -k . --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

## Implementierung

Diese Implementierung der REST-Schnittstelle biletado/assets-v2 wurde in Python 3.12 realisiert. Folgende Packages werden in den angegebenen Versionen verwendet:

- Flask 3.0.0
- Flask-Cors 4.0.0
- psycopg2 2.9.9
- PyJWT 2.8.0
- requests 2.31.0


## Konfiguration

Konfiguration der Datenbank, und des Loggings ist unter "[configuration](https://gitlab.com/Raos1/tife/-/tree/main/configuration)" vorzunehmen.

### Standard-Ports

1. Die API hört auf den Port 8000.
2. Der Keycloak-Host nutzt den Port 9090.
3. Die Datenbank läuft auf dem Port 5432.

### Auflistung aller vorkonfigurierten Parameter

| Name                     | Value    		     |
|--------------------------|-----------------|
| KEYCLOAK_HOST            | keycloak	 |
| KEYCLOAK_REALM           | biletado 		     |
| LOG_LEVEL                | INFO            |
| POSTGRES_ASSETS_USER     | postgres 		     |
| POSTGRES_ASSETS_PASSWORD | postgres 		     |
| POSTGRES_ASSETS_DBNAME   | assets   		     |
| POSTGRES_ASSETS_HOST     | postgres 		     |
| POSTGRES_ASSETS_PORT     | 5432     		     |