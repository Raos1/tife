FROM python:3.12

WORKDIR /app

COPY requirements.txt /app

RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

ENV KEYCLOAK_HOST=localhost:9090 \
    KEYCLOAK_REALM=biletado \
    POSTGRES_ASSETS_USER=postgres \
    POSTGRES_ASSETS_PASSWORD=postgres \
    POSTGRES_ASSETS_DBNAME=assets \
    POSTGRES_ASSETS_HOST=postgres \
    POSTGRES_ASSETS_PORT=5432 \
    FLASK_APP=main.py \
    FLASK_RUN_HOST=0.0.0.0 \
    FLASK_RUN_PORT=9090

EXPOSE 9090

CMD ["flask", "run"]
