# pylint: disable=missing-function-docstring

import logging
from config.logging_config import log_level
from datetime import datetime

logging.basicConfig(level=log_level)

def log_message(request=None, object_type=None, object_id=None, user_id=None, error_message=None):
    message = {
        "timestamp": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
        "request": request,
    }
    if object_type is not None and object_id is not None:
        message["object-type"] = object_type
        message["object-id"] = object_id
    if user_id is not None:
        message["user-id"] = user_id
    if error_message is not None:
        message["error-message"] = error_message

    return message