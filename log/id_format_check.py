# pylint: disable=missing-function-docstring

import uuid

def id_format_check(id):
    try:
        uuid_object = uuid.UUID(id)
        return True
    except ValueError:
        return False