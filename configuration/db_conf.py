# pylint: disable=missing-function-docstring
import os

db_params = {
    "host": os.environ.get('POSTGRES_ASSETS_HOST'),
    "port": os.environ.get('POSTGRES_ASSETS_PORT'),
    "database": os.environ.get('POSTGRES_ASSETS_DBNAME'),
    "user": os.environ.get('POSTGRES_ASSETS_USER'),
    "password": os.environ.get('POSTGRES_ASSETS_PASSWORD')
}