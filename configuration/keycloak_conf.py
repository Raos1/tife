# pylint: disable=missing-function-docstring
import os

keycloak_host = os.environ.get('KEYCLOAK_HOST')
keycloak_realm = os.environ.get('KEYCLOAK_REALM')