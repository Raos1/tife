# pylint: disable=missing-function-docstring

from flask import jsonify
import psycopg2
from psycopg2 import OperationalError
from config.db_config import db_params
from reusable.authentication import authenticate
from reusable.id_format_check import id_format_check
from datetime import datetime
from reusable.error_response import create_response
from reusable.log_message import logging, log_message

def get_building(build_id):
    if id_format_check(build_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("GET /api/v2/assets/buildings/<string:id>", "building", build_id,
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.buildings WHERE id = %s"
            cursor.execute(query, (build_id, ))
            result = cursor.fetchone()
            if result is None:
                raise OperationalError
            data = {
                "id": result[0],
                "name": result[1],
                "address": result[2]
            }
            if result[3] is not None:
                data["deleted_at"] = result[3]
        logging.info(log_message("GET /api/v2/assets/buildings/<string:id>", "building", build_id))
        return jsonify(data), 200
    except OperationalError as e:
        error_response = create_response("record not found","id does not exist")
        logging.error(log_message("GET /api/v2/assets/buildings/<string:id>", "building", build_id,
                                  error_message="id does not exist"))
        return jsonify(error_response), 404
    finally:
        if conn:
            conn.close()


def put_building(build_id, data):
    if id_format_check(build_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("PUT /api/v2/assets/buildings/<string:id>", "building",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error","no valid token")
            logging.error(log_message("PUT /api/v2/assets/buildings/<string:id>", "building",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.buildings WHERE id = %s"
            cursor.execute(query, (build_id, ))
            result = cursor.fetchone()
            if result is None:
                query = "INSERT INTO assets.public.buildings (id, name, address) VALUES (%s, %s, %s)"
                cursor.execute(query, (build_id, data['name'], data['address'].replace('\n', ' ')))
                conn.commit()

                response = {
                    "id": build_id,
                    "name": data['name'],
                    "address": data['address'].replace('\n', ' ')
                }
                logging.info(log_message("PUT /api/v2/assets/buildings/<string:id>", "building", build_id, user_id))
                return jsonify(response), 201

            elif 'deleted_at' in data:
                if data['deleted_at'] is None:
                    query = "UPDATE assets.public.buildings SET name = %s, address = %s, deleted_at = null WHERE id = %s"
                    cursor.execute(query, (data['name'], data['address'].replace('\n', ' '), build_id))
                    conn.commit()

                    response = {
                        "id": build_id,
                        "name": data['name'],
                        "address": data['address'].replace('\n', ' ')
                    }
                    logging.info(log_message("PUT /api/v2/assets/buildings/<string:id>", "building", build_id, user_id))
                    return jsonify(response), 200
                else:
                    error_response = create_response("invalid request",
                                                     "cannot delete with PUT, use DELETE")
                    logging.error(log_message("PUT /api/v2/assets/buildings/<string:id>", "building", build_id, user_id,
                                              error_message="cannot delete with PUT, use DELETE"))
                    return jsonify(error_response), 400

            elif result[3] is not None:
                error_response = create_response("invalid request",
                                                 "the building was deleted and a restore is not requested")
                logging.error(log_message("PUT /api/v2/assets/buildings/<string:id>", "building", build_id, user_id,
                                          error_message="the building was deleted and a restore is not requested"))
                return jsonify(error_response), 400

            else: # Update (Replace)
                query = "UPDATE assets.public.buildings SET name = %s, address = %s WHERE id = %s"
                cursor.execute(query, (data['name'], data['address'].replace('\n', ' '), build_id))
                conn.commit()

                response = {
                    "id": build_id,
                    "name": data['name'],
                    "address": data['address'].replace('\n', ' ')
                }
                logging.info(log_message("PUT /api/v2/assets/buildings/<string:id>", "building", build_id, user_id))
                return jsonify(response), 200

    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("PUT /api/v2/assets/buildings/<string:id>", "building",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

def get_buildings(include_deleted):
    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            if include_deleted:
                cursor.execute("SELECT * FROM assets.public.buildings")
            else:
                cursor.execute("SELECT * FROM assets.public.buildings WHERE deleted_at IS NULL")
            result = cursor.fetchall()
            buildings = {
                "buildings": []
            }
            for row in result:
                data = {
                    "id": row[0],
                    "name": row[1],
                    "address": row[2]
                }
                if row[3] is not None:
                    data["deleted_at"] = row[3]
                buildings["buildings"].append(data)
        logging.info(log_message("GET /api/v2/assets/buildings/", "building"))
        return buildings, 200
    except OperationalError as e:
        return {"buildings": []}, 200
    finally:
        if conn:
            conn.close()


def post_buildings(data):
    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error","no valid token")
            logging.error(log_message("POST /api/v2/assets/buildings", "building",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "INSERT INTO assets.public.buildings (name, address) VALUES (%s, %s) RETURNING id"
            cursor.execute(query, (data['name'], data['address'].replace('\n', ' ')))
            building_id = cursor.fetchone()[0]
        conn.commit()

        response = {
            "id": building_id,
            "name": data['name'],
            "address": data['address'].replace('\n', ' ')
        }
        logging.info(log_message("POST /api/v2/assets/buildings", "building", building_id, user_id))
        return jsonify(response), 201
    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("POST /api/v2/assets/buildings", "building",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

def delete_building(build_id):
    if id_format_check(build_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error","no valid token")
            logging.error(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.buildings WHERE id = %s"
            cursor.execute(query, (build_id, ))
            building_result = cursor.fetchone()

            query = "SELECT * FROM assets.public.storeys WHERE building_id = %s AND deleted_at IS NULL"
            cursor.execute(query, (build_id, ))
            storey_result = cursor.fetchone()
            if building_result is None or building_result[3] is not None:
                error_response = create_response("incorrect request body",
                                                 "not found or already deleted")
                logging.error(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building",
                                          error_message="not found or already deleted"))
                return jsonify(error_response), 404
            elif storey_result is not None:  # falls noch aktive storeys
                error_response = create_response("not deletable","this building has existing storeys")
                logging.error(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building",
                                          error_message="this building has existing storeys"))
                return jsonify(error_response), 400
            else:
                formatted_time = datetime.utcnow()
                query = "UPDATE assets.public.buildings SET deleted_at = %s WHERE id = %s"
                cursor.execute(query, (formatted_time, build_id))
                conn.commit()
                logging.info(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building", build_id, user_id))
                return "", 204

    except Exception as e:
        conn.rollback()  # ggf. Rollback durchführen (bei Fehler)
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("DELETE /api/v2/assets/buildings/<string:id>", "building",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()
