# pylint: disable=missing-function-docstring

from flask import jsonify
import psycopg2
from psycopg2 import OperationalError
from config.db_config import db_params
from reusable.authentication import authenticate
from reusable.id_format_check import id_format_check
from datetime import datetime
from reusable.error_response import create_response
from reusable.log_message import logging, log_message

def get_storey(storey_id):
    if id_format_check(storey_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("GET /api/v2/assets/storeys/<string:id>", "storey", storey_id,
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.storeys WHERE id = %s"
            cursor.execute(query, (storey_id, ))
            result = cursor.fetchone()
            if result is None:
                raise OperationalError
            data = {
                "id": result[0],
                "name": result[1],
                "building_id": result[2]
            }
            if result[3] is not None:
                data["deleted_at"] = result[3]
        logging.info(log_message("GET /api/v2/assets/storeys/<string:id>", "storey", storey_id))
        return jsonify(data), 200
    except OperationalError as e:
        error_response = create_response("record not found","id does not exist")
        logging.error(log_message("GET /api/v2/assets/storeys/<string:id>", "storey", storey_id,
                                  error_message="id does not exist"))
        return jsonify(error_response), 404
    finally:
        if conn:
            conn.close()


def put_storey(storey_id, data):
    if id_format_check(storey_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    if id_format_check(data['building_id']) is False:  # Formatcheck von ID
        error_response = create_response("wrong format", "building id input is not uuid format")
        logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey",
                                  error_message="building id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error","no valid token")
            logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.buildings WHERE id = %s AND deleted_at IS NULL"
            cursor.execute(query, (data['building_id'], ))
            building = cursor.fetchone()
            if building is None:
                error_response = create_response("invalid request",
                                                 "the building does not exist or was deleted")
                logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id,
                                          error_message="the building does not exist or was deleted"))
                return jsonify(error_response), 400

            query = "SELECT * FROM assets.public.storeys WHERE id = %s"
            cursor.execute(query, (storey_id, ))
            result = cursor.fetchone()
            if result is None:
                query = "INSERT INTO assets.public.storeys (id, name, building_id) VALUES (%s, %s, %s)"
                cursor.execute(query, (storey_id, data['name'], data['building_id']))
                conn.commit()

                response = {
                    "id": storey_id,
                    "name": data['name'],
                    "building_id": data['building_id']
                }
                logging.info(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id))
                return jsonify(response), 201

            elif 'deleted_at' in data:
                if data['deleted_at'] is None:
                    query = "UPDATE assets.public.storeys SET name = %s, building_id = %s, deleted_at = null WHERE id = %s"
                    cursor.execute(query, (data['name'], data['building_id'], storey_id))
                    conn.commit()
                    response = {
                        "id": storey_id,
                        "name": data['name'],
                        "building_id": data['building_id']
                    }
                    logging.info(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id))
                    return jsonify(response), 200
                else:
                    error_response = create_response("invalid request",
                                                     "cannot delete with PUT, use DELETE")
                    logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id,
                                              error_message="cannot delete with PUT, use DELETE"))
                    return jsonify(error_response), 400

            elif result[3] is not None:
                error_response = create_response("invalid request",
                                                 "the storey was deleted and a restore is not requested")
                logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id,
                                          error_message="the storey was deleted and a restore is not requested"))
                return jsonify(error_response), 400

            else:
                query = "UPDATE assets.public.storeys SET name = %s, building_id = %s WHERE id = %s"
                cursor.execute(query, (data['name'], data['building_id'], storey_id))
                conn.commit()

                response = {
                    "id": storey_id,
                    "name": data['name'],
                    "building_id": data['building_id']
                }
                logging.info(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id))
                return jsonify(response), 200

    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("PUT /api/v2/assets/storeys/<string:id>", "storey",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

def get_storeys(include_deleted, build_id):
    if id_format_check(build_id) is False:
        error_response = create_response("wrong format","building-id input is not uuid format")
        logging.error(log_message("GET /api/v2/assets/storeys>", "storey", build_id,
                                  error_message="building-id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            if include_deleted:
                query = "SELECT * FROM assets.public.storeys WHERE building_id = %s"
                cursor.execute(query, (build_id, ))
            else:
                query = "SELECT * FROM assets.public.storeys WHERE building_id = %s AND deleted_at IS NULL"
                cursor.execute(query, (build_id, ))
            result = cursor.fetchall()
            storeys = {
                "storeys": []
            }
            for row in result:
                data = {
                    "id": row[0],
                    "name": row[1],
                    "building_id": row[2]
                }
                if row[3] is not None:
                    data["deleted_at"] = row[3]
                storeys["storeys"].append(data)
                logging.info(log_message("GET /api/v2/assets/storeys/", "storey"))
        return storeys, 200
    except OperationalError as e:
        return {"storeys": []}, 200
    finally:
        if conn:
            conn.close()

def post_storeys(data):
    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error", "no valid token")
            logging.error(log_message("POST /api/v2/assets/storeys", "storey",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "INSERT INTO assets.public.storeys (name, building_id) VALUES (%s, %s) RETURNING id"
            cursor.execute(query, (data['name'], data['building_id']))
            storeys_id = cursor.fetchone()[0]
        conn.commit()

        response = {
            "id": storeys_id,
            "name": data['name'],
            "building_id": data['building_id']
        }
        logging.info(log_message("POST /api/v2/assets/storeys", "storey", storeys_id, user_id))
        return jsonify(response), 201
    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input", "incorrect request body")
        logging.error(log_message("POST /api/v2/assets/storeys", "storey",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()
            
def delete_storey(storey_id):
    if id_format_check(storey_id) is False:
        error_response = create_response("wrong format", "id input is not uuid format")
        logging.error(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error", "no valid token")
            logging.error(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.storeys WHERE id = %s"
            cursor.execute(query, (storey_id, ))
            storey_result = cursor.fetchone()

            query = "SELECT * FROM assets.public.rooms WHERE storey_id = %s AND deleted_at Is NULL"
            cursor.execute(query, (storey_id, ))
            room_result = cursor.fetchone()
            if storey_result is None or storey_result[3] is not None:
                error_response = create_response("incorrect request body",
                                                 "not found or already deleted")
                logging.error(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey",
                                          error_message="not found or already deleted"))
                return jsonify(error_response), 404
            elif room_result is not None:
                error_response = create_response("not deletable", "this storey has existing rooms")
                logging.error(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey",
                                          error_message="this storey has existing rooms"))
                return jsonify(error_response), 400
            else:
                formatted_time = datetime.utcnow()
                query = "UPDATE assets.public.storeys SET deleted_at = %s WHERE id = %s"
                cursor.execute(query, (formatted_time, storey_id))
                conn.commit()
                logging.info(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey", storey_id, user_id))
                return "", 204

    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input", "incorrect request body")
        logging.error(log_message("DELETE /api/v2/assets/storeys/<string:id>", "storey",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

