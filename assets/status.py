# pylint: disable=missing-function-docstring

from flask import jsonify
import psycopg2
from psycopg2 import OperationalError
from psycopg2 import sql
from config.db_config import db_params
from reusable.error_response import create_response
from reusable.log_message import logging, log_message


def get_status():
    status_data = {
        "authors": [
            "Timo Huter",
            "Felix Steinhauser",
        ],
        "api_version": "2.0.0"
    }
    logging.info(log_message("GET /api/v2/assets/status/"))
    return jsonify(status_data), 200


def get_health():
    database_live = check_database_live()
    database_ready = check_database_ready()

    if database_live and database_ready:
        health_data = {
            "live": True,
            "ready": True,
            "databases": {
                "assets": {
                    "connected": True
                }
            }
        }
        logging.info(log_message("GET /api/v2/assets/health/"))
        return jsonify(health_data), 200
    else:
        error_response = create_response("service is not live or not ready",
                                         "it should be replaced or restarted")
        logging.error(log_message("GET /api/v2/assets/health/",
                                  error_message="service is not live or not ready"))
        return jsonify(error_response), 503


def get_health_live():
    database_live = check_database_live()

    if database_live:
        health_live_data = {
            "live": True,
        }
        logging.info(log_message("GET /api/v2/assets/health/live/"))
        return jsonify(health_live_data), 200
    else:
        error_response = create_response("service is not live","it should be replaced or restarted")
        logging.error(log_message("GET /api/v2/assets/health/live",
                                  error_message="service is not live"))
        return jsonify(error_response), 503


def check_database_live():
    conn = None

    try:
        conn = psycopg2.connect(**db_params)
        return True

    except OperationalError as e:
        return False

    finally:
        if conn:
            conn.close()


def get_health_ready():
    database_ready = check_database_ready()

    if database_ready:
        health_ready_data = {
            "ready": True
        }
        logging.info(log_message("GET /api/v2/assets/health/ready/"))
        return jsonify(health_ready_data), 200
    else:
        error_response = create_response("service is not ready","it should be replaced or restarted")
        logging.error(log_message("GET /api/v2/assets/health/ready",
                                  error_message="service is not ready"))
        return jsonify(error_response), 503


def check_database_ready():
    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            cursor.execute(sql.SQL("SELECT * FROM assets.public.buildings LIMIT 1"))
            cursor.execute(sql.SQL("SELECT * FROM assets.public.rooms LIMIT 1"))
            cursor.execute(sql.SQL("SELECT * FROM assets.public.storeys LIMIT 1"))
        return True

    except OperationalError as e:
        return False

    finally:
        if conn:
            conn.close()
