# pylint: disable=missing-function-docstring

from flask import jsonify
import psycopg2
from psycopg2 import OperationalError
from config.db_config import db_params
from reusable.authentication import authenticate
from reusable.id_format_check import id_format_check
from datetime import datetime
from reusable.error_response import create_response
from reusable.log_message import logging, log_message

def get_room(room_id):
    if id_format_check(room_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("GET /api/v2/assets/rooms/<string:id>", "room", room_id,
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400
    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.rooms WHERE id = %s"
            cursor.execute(query, (room_id, ))
            result = cursor.fetchone()
            if result is None:
                raise OperationalError
            data = {
                "id": result[0],
                "name": result[1],
                "storey_id": result[2]
            }
            if result[3] is not None:
                data["deleted_at"] = result[3]
        logging.info(log_message("GET /api/v2/assets/rooms/<string:id>", "room", room_id))
        return jsonify(data), 200
    except OperationalError as e:
        error_response = create_response("record not found","id does not exist")
        logging.error(log_message("GET /api/v2/assets/rooms/<string:id>", "room", room_id,
                                  error_message="id does not exist"))
        return jsonify(error_response), 404
    finally:
        if conn:
            conn.close()


def put_room(room_id, data):
    if id_format_check(room_id) is False:
        error_response = create_response("wrong format","id input is not uuid format")
        logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400
    if id_format_check(data['storey_id']) is False:  # Formatcheck von ID
        error_response = create_response("wrong format", "storey id input is not uuid format")
        logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error","no valid token")
            logging.error(log_message("PUT /api/v2/assets/room/<string:id>", "room",
                                      error_message="no valid token"))
            return jsonify(error_data), 401
        conn = psycopg2.connect(**db_params)
        data_storey_id = data['storey_id']
        data_name = data['name']
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.storeys WHERE id = %s AND deleted_at IS NULL"
            cursor.execute(query, (data_storey_id, ))
            storey = cursor.fetchone()
            if storey is None:
                error_response = create_response("invalid request",
                                                "the storey does not exist or was deleted")
                logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id,
                                        error_message="the storey does not exist or was deleted"))
                return jsonify(error_response), 400
            query = "SELECT * FROM assets.public.rooms WHERE id = %s"
            cursor.execute(query, (room_id, ))
            result = cursor.fetchone()
            if result is None:
                query = "INSERT INTO assets.public.rooms (id, name, storey_id) VALUES (%s, %s, %s)"
                cursor.execute(query, (room_id, data_name, data_storey_id))
                conn.commit()

                response = {
                    "id": room_id,
                    "name": data_name,
                    "storey_id": data_storey_id
                }
                logging.info(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id))
                return jsonify(response), 201

            elif 'deleted_at' in data:
                data_deleted_at = data['deleted_at']
                if data_deleted_at is None:
                    query = "UPDATE assets.public.rooms SET name = %s, storey_id = %s, deleted_at = null WHERE id = %s"
                    cursor.execute(query, (data['name'], data['storey_id'], room_id))
                    conn.commit()

                    response = {
                        "id": room_id,
                        "name": data_name,
                        "storey_id": data_storey_id
                    }
                    logging.info(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id))
                    return jsonify(response), 200
                else:
                    error_response = create_response("invalid request",
                                                "cannot delete with PUT, use DELETE")
                    logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id,
                                                error_message="cannot delete with PUT, use DELETE"))
                    return jsonify(error_response), 400

            elif result[3] is not None:
                error_response = create_response("invalid request",
                                                "the room was deleted and a restore is not requested")
                logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id,
                                        error_message="the room was deleted and a restore is not requested"))
                return jsonify(error_response), 400

            else:
                query = "UPDATE assets.public.rooms SET name = %s, storey_id = %s WHERE id = %s"
                cursor.execute(query, (data_name, data_storey_id, room_id))
                conn.commit()

                response = {
                    "id": room_id,
                    "name": data_name,
                    "storey_id": data_storey_id
                }
                logging.info(log_message("PUT /api/v2/assets/rooms/<string:id>", "room", room_id, user_id))
                return jsonify(response), 200

    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("PUT /api/v2/assets/rooms/<string:id>", "room",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

def get_rooms(include_deleted, storey_id):
    if (not storey_id):
        conn = None
        try:
            conn = psycopg2.connect(**db_params)
            with conn.cursor() as cursor:
               if include_deleted:
                   query = "SELECT * FROM assets.public.rooms"
                   cursor.execute(query)
               else:
                   query = "SELECT * FROM assets.public.rooms WHERE deleted_at Is NULL"
                   cursor.execute(query)
                result = cursor.fetchall()
                rooms = {
                    "rooms": []
                }
                for row in result:
                    data = {
                        "id": row[0],
                        "name": row[1],
                        "storey_id": row[2]
                    }
                    if row[3] is not None:
                        data["deleted_at"] = row[3]
                    rooms["rooms"].append(data)
            logging.info(log_message("GET /api/v2/assets/rooms/", "room"))
            return rooms, 200
        

    if id_format_check(storey_id) is False:

        error_response = create_response("wrong format","storey-id input is not uuid format")
        logging.error(log_message("GET /api/v2/assets/rooms>", "room", storey_id,
                                  error_message="storey-id input is not uuid format"))
        return jsonify(error_response), 400

    conn = None
    try:
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            if include_deleted:
                query = "SELECT * FROM assets.public.rooms WHERE storey_id = %s"
                cursor.execute(query, (storey_id, ))
            else:
                query = "SELECT * FROM assets.public.rooms WHERE storey_id = %s AND deleted_at Is NULL"
                cursor.execute(query, (storey_id, ))
            result = cursor.fetchall()
            rooms = {
                "rooms": []
            }
            for row in result:
                data = {
                    "id": row[0],
                    "name": row[1],
                    "storey_id": row[2]
                }
                if row[3] is not None:
                    data["deleted_at"] = row[3]
                rooms["rooms"].append(data)
        logging.info(log_message("GET /api/v2/assets/rooms/", "room"))
        return rooms, 200
    except OperationalError as e:
        return {"rooms": []}, 200
    finally:
        if conn:
            conn.close()


def post_rooms(data):
    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error", "no valid token")
            logging.error(log_message("POST /api/v2/assets/rooms", "room",
                                      error_message="no valid token"))
            return jsonify(error_data), 401

        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "INSERT INTO assets.public.rooms (name, storey_id) VALUES (%s, %s) RETURNING id"
            cursor.execute(query, (data['name'], data['storey_id']))
            room_id = cursor.fetchone()[0]
        conn.commit()
        response = {
            "id": room_id,
            "name": data['name'],
            "storey_id": data['storey_id']
        }
        logging.info(log_message("POST /api/v2/assets/rooms", "room", data['storey_id'], user_id))
        return jsonify(response), 201
    except Exception as e:
        conn.rollback()  # ggf. Rollback durchführen (bei Fehler)
        error_response = create_response("invalid input", "incorrect request body")
        logging.error(log_message("POST /api/v2/assets/rooms", "room",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()

def delete_room(room_id):
    if id_format_check(room_id) is False:
        error_response = create_response("wrong format", "id input is not uuid format")
        logging.error(log_message("DELETE /api/v2/assets/rooms/<string:id>", "room",
                                  error_message="id input is not uuid format"))
        return jsonify(error_response), 400
    conn = None
    try:
        user_id = authenticate()
        if user_id is None:
            error_data = create_response("authentication error", "no valid token")
            logging.error(log_message("DELETE /api/v2/assets/rooms/<string:id>", "room",
                                      error_message="no valid token"))
            return jsonify(error_data), 401
        conn = psycopg2.connect(**db_params)
        with conn.cursor() as cursor:
            query = "SELECT * FROM assets.public.rooms WHERE id = %s"
            cursor.execute(query, (room_id, ))
            room_result = cursor.fetchone()
            if room_result is None or room_result[3] is not None:
                error_response = create_response("incorrect request body",
                                                "not found or already deleted")
                logging.error(log_message("DELETE /api/v2/assets/rooms/<string:id>", "room",
                                        error_message="not found or already deleted"))
                return jsonify(error_response), 404
            else:
                formatted_time = datetime.utcnow()
                query = "UPDATE assets.public.rooms SET deleted_at = %s WHERE id = %s"
                cursor.execute(query, (formatted_time, room_id))
                conn.commit()
                logging.info(log_message("DELETE /api/v2/assets/rooms/<string:id>", "room", room_id, user_id))
            return "", 204
    except Exception as e:
        conn.rollback()
        error_response = create_response("invalid input","incorrect request body")
        logging.error(log_message("DELETE /api/v2/assets/rooms/<string:id>", "room",
                                  error_message="incorrect request body"))
        return jsonify(error_response), 400
    finally:
        if conn:
            conn.close()
